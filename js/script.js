class Hamburger {
  constructor(size, stuffing) {
    this._size = size;
    this._stuffing = stuffing;
    this._price = size[0] + stuffing[0];
    this._calories = size[1] + stuffing[1];
    this._toppings = [];
  }

  set topping(hamburgerTopping) {
    if (hamburgerTopping[2] !== "TOPPING_MAYO" && hamburgerTopping[2] !== "TOPPING_SPICE") {
      return;
    }
    try {
      for (let i = 0; i <= this._toppings.length; i++) {
        if (this._toppings[i] === hamburgerTopping[2]) {
          this._price -= hamburgerTopping[0];
          this._calories -= hamburgerTopping[1];
          this._toppings[i] = "";
          throw new Error("duplicate");
        }
      }
    } catch (e) {
      hamburgerException(e.message, hamburgerTopping[2]);
    }
    this._toppings.push(hamburgerTopping[2]);
    this._price += hamburgerTopping[0];
    this._calories += hamburgerTopping[1];
  }

  get topping() {
    let toppings = [];
    for (let i = 0; i <= this._toppings.length; i++) {
      if (this._toppings[i] === "TOPPING_MAYO") {
        toppings.push(this._toppings[i]);
      }
      if (this._toppings[i] === "TOPPING_SPICE") {
        toppings.push(this._toppings[i]);
      }
    }
    console.log(toppings);
  }

  removeTopping(hamburgerTopping) {
    if (hamburgerTopping[2] !== "TOPPING_MAYO" && hamburgerTopping[2] !== "TOPPING_SPICE") {
      return;
    }
    try {
      for (let i = 0; i <= this._toppings.length; i++) {
        if (this._toppings[i] === hamburgerTopping[2]) {
          this._price -= hamburgerTopping[0];
          this._calories -= hamburgerTopping[1];
          this._toppings[i] = "";
          return;
        }
      }
      for (let i = 0; i <= this._toppings.length; i++) {
        if (this._toppings[i] !== hamburgerTopping[2]) {
          throw new Error("already deleted");
        }
      }
    } catch (e) {
      hamburgerException(e.message, hamburgerTopping[2]);
    }
  }

  get size() {
    console.log(this._size[2]);
  }

  get stuffing() {
    console.log(this._stuffing[2]);
  }

  get price() {
    console.log(`${this._price * 94}`);
  }

  get calories() {
    console.log(`${this._calories}`);
  }
}

let hamburgerException = (e, topping) => {
  if (e === "duplicate") {
    console.log(`duplicate topping: '${topping}'`);
  }
  if (e === "already deleted") {
    console.log(`'${topping}' is already deleted`);
  }
};

Hamburger.SIZE_SMALL = [50, 20, "SIZE_SMALL"];
Hamburger.SIZE_LARGE = [100, 40, "SIZE_LARGE"];
Hamburger.STUFFING_CHEESE = [10, 20, "STUFFING_CHEESE"];
Hamburger.STUFFING_SALAD = [20, 5, "STUFFING_SALAD"];
Hamburger.STUFFING_POTATO = [15, 10, "STUFFING_POTATO"];
Hamburger.TOPPING_MAYO = [20, 5, "TOPPING_MAYO"];
Hamburger.TOPPING_SPICE = [15, 0, "TOPPING_SPICE"];

let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

hamburger.topping = Hamburger.TOPPING_MAYO;
hamburger.topping = Hamburger.TOPPING_MAYO;
hamburger.topping = Hamburger.TOPPING_SPICE;
hamburger.topping = Hamburger.TOPPING_SPICE;
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
hamburger.topping;
hamburger.size;
hamburger.stuffing;
hamburger.price;
hamburger.calories;